## puppet-sudo  
This sudo module is designed to give sudo privileges to users.(no checks if the user is actually a unix account)

### Parameters to sudo class
* `sudo_user`: Give sudo privileges to a list of users, default, locmap will give to the responsible user sudo privileges.

Puppet databindings allows the parameters to be set via hiera, users can override the default values inside '/etc/puppet/userdata/module_names/sudo/sudo.yaml

```YAML
---
sudo_users:
- user1
- user2
``` 
*Writing in /etc/puppet/userdata/module_names/sudo/sudo.yaml means that locmap behaviour is overriden, dont forget to include ALL the accounts that you want.*

## Contact
Aris Boutselis <aris.boutselis@cern.ch>  

## Support
https://gitlab.cern.ch/linuxsupport/puppet-sudo