class sudo::params {

  $owner          = 'root'
  $group          = 'root'
  $mode           = '0640'
  $sudoersfile    = '/etc/sudoers'
  $sudodirectory  = '/etc/sudoers.d'
  $sudotemplate   = 'sudo/sudo-users.rb'
  $sudofile       = '/etc/sudoers.d/000-sudo-users'
  $sudoline       = '#includedir /etc/sudoers.d'
  $ensuredir      = 'directory'
  $sudoersdirpath = '/etc/sudoers.d'
  $users = lookup({"name" => "sudo_users", "default_value" => []})
}
