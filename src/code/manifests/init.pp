class sudo {

  class {'sudo::install':}
  class {'sudo::params':}
  class {'sudo::config':}

}
