class sudo::config {

  file { 'sudoers':
    ensure  => present,
    path    => $sudo::params::sudoersfile,
    owner   => $sudo::params::owner,
    group   => $sudo::params::group,
    mode    => $sudo::params::mode,
    require => Class['sudo::install'],
  }

  # SLC6 and CC7 has 1.8.6 so sudoers.d exists when package is installed
  file { 'sudoers.d':
    ensure  => $sudo::params::ensuredir,
    path    => $sudo::params::sudoersdirpath,
    owner   => $sudo::params::owner,
    group   => $sudo::params::group,
    mode    => $sudo::params::mode,
    require => File[$sudo::params::sudoersfile],
  }

  file { '000-sudo-users':
    ensure  => present,
    path    => $sudo::params::sudofile,
    owner   => $sudo::params::owner,
    group   => $sudo::params::group,
    mode    => $sudo::params::mode,
    content => template($sudo::params::sudotemplate),
  }

  exec {"sudo-syntax-check for file ${sudo::params::sudofile}":
    command     => "/usr/sbin/visudo -c -f '${sudo::params::sudofile}' || ( rm -f '${sudo::params::sudofile}' && exit 1)",
    refreshonly => true,
  }
}
