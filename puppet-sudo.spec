Name:           puppet-sudo
Version:        2.2
Release:        1%{?dist}
Summary:        Masterless puppet module for sudo

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Masterless puppet module for sudo.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/sudo/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/sudo/
touch %{buildroot}/%{_datadir}/puppet/modules/sudo/linuxsupport

%files -n puppet-sudo
%{_datadir}/puppet/modules/sudo
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.2-1
- Add autoreconfigure %post script

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.1-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- fix requires on puppet-agent

* Thu Feb 11 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- move away from hiera to lookup

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
- Rebuild for el8
 
* Wed May 02 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- New version for locmap 2.0

* Fri Dec 16 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-1
- Missing directory creation for /root/.sudo :(

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
